import Top from './Top';
import Stori from './Stori';
import Isi from './Isi';
import Bottom from './Bottom';

export { Top, Stori, Isi, Bottom };