import React, { useState } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import gambar5 from '../assets/gambar5.jpeg';
import gambar6 from '../assets/gambar6.jpeg';
import suka from '../assets/suka.png';
import komen from '../assets/komen.png';
import pesan from '../assets/pesan.png';
import koleksi from '../assets/koleksi.png';
import burger from '../assets/burger.png';

const Isi = () => {
    const [tambah, setTambah] = useState(0);
    return (
        <View>
            <View style={{flexDirection: 'row'}}>
                <Image source={gambar5} style={gaya.gambar5}/>
                <Text style={gaya.text}>Rozaini</Text>
                <Image source={burger} style={gaya.burger}/>
            </View>
            <Image source={gambar6} style={gaya.gambar6}/>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => setTambah(tambah + 1)}>
                    <Image source={suka} style={gaya.suka}/>
                </TouchableOpacity>
                <Image source={komen} style={gaya.komen}/>
                <Image source={pesan} style={gaya.pesan}/>
                <Image source={koleksi} style={gaya.koleksi}/>
            </View>
            <Text style={gaya.tex}>{tambah} Suka</Text>
            <Text style={gaya.tex}>Nadia__ Hi guys....</Text>
            <Text style={gaya.textt}>Lihat 130 komentar</Text>
        </View>
    );
};

export default Isi;

const gaya = StyleSheet.create({
    gambar5: {
        marginTop: 10,
        marginLeft: 11,
        width: 30,
        height:30,
        borderRadius:30/2,

    },

    gambar6: {
        marginTop: 10,
        width: 370,
        height:370,

    },

    suka: {
        marginTop: 10,
        marginLeft:10,
        width: 28,
        height:25,

    },

    komen: {
        marginTop: 10,
        marginLeft:10,
        width: 25,
        height:25,

    },

    pesan: {
        marginTop: 10,
        marginLeft:10,
        width: 28,
        height:25,

    },

    koleksi: {
        marginTop: 10,
        marginLeft:215,
        width: 21,
        height:25,

    },

    burger: {
        marginTop: 10,
        marginLeft:290,
        width: 3,
        height:20,

    },

    text: {
        marginTop: 15,
        marginLeft:15,
        color: 'white',

    },

    tex: {
        marginTop: 10,
        marginLeft:18,
        color: 'white',

    },

    textt: {
        marginTop: 10,
        marginLeft:18,
        color: 'silver',

    },

});