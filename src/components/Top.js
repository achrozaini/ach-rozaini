import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import nama from '../assets/nama.png';
import suka from '../assets/suka.png';
import pesan from '../assets/pesan.png';

const Top = () => {
    return (
        <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, height: 53, flexDirection: 'row', backgroundColor: 'black'}}>
                <Image source={nama} style={gaya.nama} />
                <Image source={suka} style={gaya.suka} />
                <Image source={pesan} style={gaya.pesan} />
            </View>
        </View>
    );
};

export default Top;

const gaya = StyleSheet.create({
    nama: {
        marginTop: 14,
        marginLeft:18,
        width: 106,
        height:30,
    },

    suka: {
        marginTop:14,
        marginLeft: 160,
        width: 29,
        height: 26,
    },

    pesan: {
        marginTop: 16,
        marginLeft: 25,
        width: 29,
        height: 25,
    },
});
