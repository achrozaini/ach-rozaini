import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import beranda from '../assets/beranda.png';
import pencarian from '../assets/pencarian.png';
import tambah from '../assets/tambah.png';
import belanja from '../assets/belanja.png';
import gambar5 from '../assets/gambar5.jpeg';

const Bottom = () => {
    return (
        <View style={{position: 'absolute', bottom: 364}}>
            <View style={{width: 400, height: 53, flexDirection: 'row', backgroundColor: 'black'}}>
                <Image source={beranda} style={gaya.beranda} />
                <Image source={pencarian} style={gaya.pencarian} />
                <Image source={tambah} style={gaya.tambah} />
                <Image source={belanja} style={gaya.belanja} />
                <Image source={gambar5} style={gaya.gambar5} />
            </View>
        </View>
    );
};

export default Bottom;

const gaya = StyleSheet.create({
    beranda: {
        marginTop: 9,
        marginLeft:15,
        width: 25,
        height:25,
    },

    pencarian: {
        marginTop: 9,
        marginLeft:52,
        width: 25,
        height:25,
    },

    tambah: {
        marginTop: 9,
        marginLeft:52,
        width: 25,
        height:25,
    },

    belanja: {
        marginTop: 9,
        marginLeft:52,
        width: 22,
        height:25,
    },

    gambar5: {
        marginTop: 9,
        marginLeft:52,
        width: 28,
        height:28,
        borderRadius: 28/2,
    },
});
