import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Top, Stori, Isi, Bottom } from '../components';

const App = () => {
  return (
    <View style={{backgroundColor: 'black'}}>
      <View>
        <Top />
        <Stori />
        <ScrollView>
          <Isi />
          <Isi />
          <Isi />
          <Isi />
        </ScrollView>
        <Bottom />
      </View>
    </View>
  );
};

export default App;
