import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import gambar1 from '../assets/gambar1.jpeg';
import gambar2 from '../assets/gambar2.jpeg';
import gambar3 from '../assets/gambar3.jpeg';
import gambar4 from '../assets/gambar4.jpeg';
import gambar5 from '../assets/gambar5.jpeg';

const Props = props => {
    return (
        <View>
            <Image source={props.gambar} style={gaya.gambar}/>
            <Text style={gaya.tex}>{props.tex}</Text>
        </View>
    );
};

const Stori = () => {
    return (
        <View>
            <View style={{flex: 1, height: 100, flexDirection: 'row'}}>
                <Props gambar={gambar1} />
                <Props gambar={gambar2} />
                <Props gambar={gambar3} />
                <Props gambar={gambar4} />
                <Props gambar={gambar5} />
            </View>
            <View>
                <Props tex='Cerita Anda'/>
            </View>
        </View>
    );
};

export default Stori;

const gaya = StyleSheet.create({
    gambar: {
        marginTop: 30,
        marginLeft:18,
        width: 70,
        height:70,
        borderRadius:70/2,

    },

    tex: {
        marginTop: 10,
        marginLeft: 20,
        color: 'white',

    },
});
